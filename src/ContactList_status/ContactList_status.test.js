import React from 'react';
import Status from './ContactList_status';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
const mockStore = configureStore();

describe('ContactList_status', () => {
  afterEach(() => {
    cleanup();
  });

  it('shows a loading message', () => {
    let initialState = {
      contact_list: {
        isLoading: true,
        error: null,
      },
    };
    const store = mockStore(initialState);
    const { getByText } = render(
      <Provider store={store}>
        <Status />
      </Provider>
    );
    expect(getByText(/loading/i)).toBeInTheDocument();
  });

  it('shows an error message', () => {
    let initialState = {
      contact_list: {
        isLoading: false,
        error: true,
      },
    };
    const store = mockStore(initialState);
    const { getByText } = render(
      <Provider store={store}>
        <Status />
      </Provider>
    );
    expect(getByText(/error/i)).toBeInTheDocument();
  });
});
