import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { contactList_testStatus } from '../actions';
import './ContactList_status.css';

function Status() {
  const dispatch = useDispatch(),
    [text, setText] = useState(''),
    [showStatus, setShowStatus] = useState(''),
    statusClass = 'ContactList_status absolute-element',
    isLoading = useSelector(state => state.contact_list.isLoading),
    error = useSelector(state => state.contact_list.error);

  useEffect(() => {
    if (isLoading) {
      setText('Loading...');
      setShowStatus('status--expand');
    }
    if (error) {
      setText('Error...');
      setShowStatus('status--expand');
    }
    if (!error && !isLoading) {
      setShowStatus('status--collapse');
    }
  }, [isLoading, error]);
  /*
  Testing loader
  useEffect(() => {
    setTimeout(() => dispatch(contactList_testStatus({isLoading: true})),2000)
    setTimeout(() => dispatch(contactList_testStatus({isLoading: false})),4000)
    setTimeout(() => dispatch(contactList_testStatus({isLoading: false, error:true})),6000)
    setTimeout(() => dispatch(contactList_testStatus({isLoading: false, error:false})),8000)
  }, [])
  */

  return (
    <div className={`${statusClass} ${showStatus}`}>
      <span className="ContactList_status-text">{text}</span>
    </div>
  );
}

export default Status;
