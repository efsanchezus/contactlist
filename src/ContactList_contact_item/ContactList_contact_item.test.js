import React from 'react';
import { render } from '@testing-library/react';
import { shallow } from 'enzyme';
import Item from '../ContactList_contact_item/ContactList_contact_item';

describe('ContactList_contact_item', () => {
  it('render a <li /> for SEO optimization since its parent is an <ul>', () => {
    let wrapper = shallow(<Item topic="" />);
    expect(wrapper.find('li').length).toEqual(1);
  });

  it('renders the topic and value', () => {
    const { getByText } = render(<Item topic="Topic" value="Value" />);
    expect(getByText('Topic')).toBeInTheDocument();
    expect(getByText('Value')).toBeInTheDocument();
  });
});
