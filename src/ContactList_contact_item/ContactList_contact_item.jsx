import React from 'react';
import PropTypes from 'prop-types';
import './ContactList_contact_item.css';

function ContactItem({ topic = '', value = '' }) {
  return (
    <li className="ContactList_contact_item">
      <span className="ContactList_contact_item-topic">{topic}</span>
      <span className="ContactList_contact_item-value">{value}</span>
    </li>
  );
}

ContactItem.propTypes = {
  topic: PropTypes.string.isRequired,
};
export default ContactItem;
