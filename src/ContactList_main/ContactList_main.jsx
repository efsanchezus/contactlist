import React from 'react';
import Content from '../ContactList_content/ContactList_content';
import Status from '../ContactList_status/ContactList_status';
import Contact from '../ContactList_contact/ContactList_contact';
import './ContactList_main.css';

function Main() {
  return (
    <main className="ContactList_main">
      <Content />
      <Contact />
      <Status />
    </main>
  );
}

export default Main;
