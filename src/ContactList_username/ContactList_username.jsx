import React from 'react';
import PropTypes from 'prop-types';
import './ContactList_username.css';

const Username = ({ text = '' }) => (
  <div className="ContactList_username">Username {text}</div>
);

Username.propTypes = {
  text: PropTypes.string,
};
export default Username;
