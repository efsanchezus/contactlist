import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { general_changeWidth } from '../actions';

function ViewportSize() {
  const dispatch = useDispatch(),
    onResize = time => {
      let timer = null;
      return () => {
        clearTimeout(timer);
        timer = setTimeout(() => {
          setX(getWidth());
          setY(getHeight());
        }, time);
      };
    },
    getWidth = () => {
      const w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth;
      return x;
    },
    getHeight = () => {
      const w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        y = w.innerHeight || e.clientHeight || g.clientHeight;
      return y;
    },
    [x, setX] = useState(getWidth()),
    [y, setY] = useState(getHeight());

  useEffect(() => {
    dispatch(general_changeWidth({ width: x }));
    window.addEventListener('resize', onResize(200), false);
    return () => {
      window.removeEventListener('resize', onResize(200), false);
    };
  });
  return <></>;
}

export default ViewportSize;
