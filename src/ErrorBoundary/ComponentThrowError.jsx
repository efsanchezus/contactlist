import React, { useEffect } from 'react';

function ComponentThrowError() {
  useEffect(() => {
    throw Error('My error');
  }, []);
  return <div></div>;
}

export default ComponentThrowError;
