import React from 'react';
import ErrorBoundary from './ErrorBoundary';
import ComponentThrowError from './ComponentThrowError';
import { cleanup, render } from '@testing-library/react';

describe('ErrorBoundary', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders props.children if there is no error', () => {
    const data = {
      message: 'An error',
      textContent: 'Component',
    };

    const { queryByText } = render(
      <ErrorBoundary text={data.message}>
        <div>{data.textContent}</div>
      </ErrorBoundary>
    );
    expect(queryByText(data.textContent)).toBeInTheDocument();
  });

  it('shows an error message if there is an error', () => {
    const message = 'An error';
    const { getByText } = render(
      <ErrorBoundary text={message}>
        <ComponentThrowError />
      </ErrorBoundary>
    );
    expect(getByText(message)).toBeInTheDocument();
  });
});
