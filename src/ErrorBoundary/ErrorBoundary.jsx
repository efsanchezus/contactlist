import React from 'react';
import './ErrorBoundary.css';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }
  componentDidCatch(error, info) {
    //Log to service error and info
    //console.log("error: ", error.message, info)
  }
  render() {
    const { hasError } = this.state,
      { text } = this.props;
    if (hasError) {
      return <p className="ErrorBoundary">{text}</p>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
