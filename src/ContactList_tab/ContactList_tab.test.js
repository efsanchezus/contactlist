import React from 'react';
import Tab from './ContactList_tab';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

const store = mockStore(initialState);
store.dispatch = jest.fn();

describe('ContactList_tab', () => {
  let getByText;
  beforeEach(() => {
    ({ getByText } = render(
      <Provider store={store}>
        <Tab letter="a" />
      </Provider>
    ));
  });

  afterEach(() => {
    cleanup();
  });

  it('renders the tab letter and counter', () => {
    expect(getByText('a')).toBeInTheDocument();
    expect(getByText('2')).toBeInTheDocument();
  });

  it('calls dispatch when tab is clicked', () => {
    fireEvent.click(getByText('a'));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
