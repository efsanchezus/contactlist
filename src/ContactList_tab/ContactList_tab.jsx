import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { contactList_changeLetter } from '../actions';
import {
  isActiveLetterSelector as isActiveLetter,
  isNormalLetterSelector as isNormalLetter,
  countUsersInLetterSelector as countUsersInLetter,
} from './selectors';
import PropTypes from 'prop-types';
import './ContactList_tab.css';

function Tab({ letter }) {
  const count = useSelector(countUsersInLetter(letter)),
    isActive = useSelector(isActiveLetter(letter)),
    isNormal = useSelector(isNormalLetter(letter)),
    tabActiveModif = isActive
      ? 'ContactList_tab_active--show'
      : 'ContactList_tab_active--hide',
    isNormalModif = isNormal
      ? 'ContactList_tab--normal'
      : 'ContactList_tab--disabled',
    tabActiveClass = `ContactList_tab_active absolute-element ${tabActiveModif}`,
    tabInnerClass = `ContactList_tab_inner absolute-element ${isNormalModif}`,
    dispatch = useDispatch(),
    changeLetter = () => {
      if (!isNormal) {
        return;
      }
      dispatch(contactList_changeLetter(letter));
    },
    callback = useCallback(changeLetter, [dispatch, letter, isNormal]);

  return (
    <div className="ContactList_tab" onClick={callback}>
      <div className={tabInnerClass}>
        <span className="ContactList_tab_letter">{letter}</span>
        <span className="ContactList_tab_counter">{count}</span>
      </div>
      <div className={tabActiveClass} />
      <div className="ContactList_tab ContactList_tab_trick" />
    </div>
  );
}

Tab.propTypes = {
  letter: PropTypes.string.isRequired,
};
export default Tab;
