import { createSelector } from 'reselect';

const isActiveLetterSelector = letter => state =>
  state.contact_list.currentLetter === letter;
const isNormalLetterSelector = letter => state =>
  state.contact_list.lettersWithData.includes(letter);
const countUsersInLetterSelector = letter => state =>
  state.contact_list.usersByLetter[letter]
    ? state.contact_list.usersByLetter[letter].length
    : 0;

export {
  isActiveLetterSelector,
  isNormalLetterSelector,
  countUsersInLetterSelector,
};
