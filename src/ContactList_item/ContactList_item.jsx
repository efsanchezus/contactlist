import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { contactList_changeCurrentUser } from '../actions';
import './ContactList_item.css';

function Item({ id, name }) {
  const dispatch = useDispatch(),
    changeCurrentUser = () => dispatch(contactList_changeCurrentUser(id)),
    callback = useCallback(changeCurrentUser, [dispatch, id]);

  return (
    <div className="ContactList_item" onClick={callback}>
      <span className="ContactList_item-text">{name}</span>
      <div className="ContactList_item-background absolute-element" />
    </div>
  );
}

Item.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
export default Item;
