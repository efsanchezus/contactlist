import React from 'react';
import Item from './ContactList_item';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
const mockStore = configureStore();

describe('ContactList_item', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders the name and calls dispatch if item is clicked', () => {
    const store = mockStore();
    store.dispatch = jest.fn();
    const { getByText } = render(
      <Provider store={store}>
        <Item id="a" name="Emiliano" />
      </Provider>
    );
    expect(getByText(/emiliano/i)).toBeInTheDocument();

    fireEvent.click(getByText(/emiliano/i));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
