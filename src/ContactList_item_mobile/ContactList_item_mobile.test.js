import React from 'react';
import Item from './ContactList_item_mobile';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_item_mobile', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders the name and calls dispatch if item is clicked', () => {
    const store = mockStore(initialState);
    store.dispatch = jest.fn();
    const { getByText } = render(
      <Provider store={store}>
        <Item id="9832ff03-ef05-4614-877f-ce6787bd9ccf" />
      </Provider>
    );
    expect(getByText(/felecia/i)).toBeInTheDocument();

    fireEvent.click(getByText(/felecia/i));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
