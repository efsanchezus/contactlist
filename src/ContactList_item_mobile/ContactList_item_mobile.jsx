import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { contactList_changeCurrentUser } from '../actions';
import { useSelector, useDispatch } from 'react-redux';
import './ContactList_item_mobile.css';

let mockUser = {
  name: {},
};
function Item({ id }) {
  const dispatch = useDispatch(),
    changeCurrentUser = () => dispatch(contactList_changeCurrentUser(id)),
    callback = useCallback(changeCurrentUser, [dispatch, id]),
    user = useSelector(state => state.contact_list.usersById[id] || mockUser),
    last = user.name.last || '',
    first = user.name.first || '';

  return (
    <div className="ContactList_item_mobile" onClick={callback}>
      <div className="ContactList_item_mobile-background absolute-element" />
      <div className="ContactList_item_mobile-text">
        {last.toUpperCase()}, {first}
      </div>
    </div>
  );
}

Item.propTypes = {
  id: PropTypes.string.isRequired,
};
export default Item;
