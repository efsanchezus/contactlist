import React from 'react';
import ContactList from './ContactList';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList', () => {
  afterEach(() => {
    cleanup();
  });

  test('DESKTOP. Renders A-Z in tabs with its counter + names starting with the letter selected', () => {
    const _el = document.createElement('div');
    _el.classList.add('loader__box');
    document.body.appendChild(_el);
    const store = mockStore({
      contact_list: { ...initialState.contact_list, currentLetter: 'a' },
      general: { width: 900 },
    });
    store.dispatch = jest.fn();
    const { getByText, queryByText, queryAllByText } = render(
      <Provider store={store}>
        <ContactList />
      </Provider>
    );

    expect(getByText('a')).toBeInTheDocument();
    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();

    expect(queryAllByText('2').length).toEqual(2);
    expect(getByText('z')).toBeInTheDocument();
    expect(queryByText(/brewer/i)).toBeNull();
  });

  test('MOBILE. Renders just letters that have data + names starting with the letter selected', () => {
    const _el = document.createElement('div');
    _el.classList.add('loader__box');
    document.body.appendChild(_el);
    const store = mockStore({
      contact_list: { ...initialState.contact_list, currentLetter: 'a' },
      general: { width: 600 },
    });
    store.dispatch = jest.fn();
    const { getByText, queryByText, queryAllByText } = render(
      <Provider store={store}>
        <ContactList />
      </Provider>
    );

    expect(getByText('a')).toBeInTheDocument();
    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();

    expect(queryAllByText('2').length).toEqual(0);
    expect(queryByText('z')).toBeNull();
    expect(getByText(/brewer/i)).toBeInTheDocument();
    expect(getByText('b')).toBeInTheDocument();
  });
});
