import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchThenCreateLists_thunk } from '../actions';
import Header from '../ContactList_header/ContactList_header';
import Main from '../ContactList_main/ContactList_main';
import Main_mobile from '../ContactList_main_mobile/ContactList_main_mobile';
import './ContactList.css';

function ContactList() {
  const dispatch = useDispatch(),
    [isReady, setIsReady] = useState(false),
    width = useSelector(state => state.general.width);

  useEffect(() => {
    setIsReady(true);
  }, []);

  useEffect(() => {
    const _el = document.querySelector('.loader__box');
    _el.classList.add('loader__box--expand');
  }, []);

  useEffect(() => {
    let start = performance.now();

    dispatch(
      fetchThenCreateLists_thunk(configJson.userUrl, '?results=100&nat=us', 800)
    );

    let end = performance.now();
    let time = end - start;
    if (time > 3)
      console.log('ContactList Fetching taking more than 3ms', end - start);
  }, []);

  return (
    <section className={`ContactList ${isReady ? 'ContactList--ready' : ''}`}>
      {width >= 800 && (
        <React.Fragment>
          <Header tabs={configJson['tabs']} />
          <Main />
        </React.Fragment>
      )}
      {width < 800 && <Main_mobile />}
    </section>
  );
}

let configJson = {
  title: 'Contact List',
  userUrl: 'https://api.randomuser.me/',
  numberCards: 120,
  tabs: [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
  ],
};

export default ContactList;
