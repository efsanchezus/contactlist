import { createSelector } from 'reselect';
const getCurrentLetter = state => state.contact_list.currentLetter;
const getLeftPosition = letter => state =>
  state.contact_list.listsWidth[letter];
const getNames = () =>
  createSelector(
    state => state.contact_list.usersById,
    state => state.contact_list.usersByLetter,
    (_, letter) => letter,
    (usersById, usersByLetter, letter) =>
      usersByLetter[letter].map(id => {
        let user = usersById[id];
        let uid = user.login.uuid;
        let first = user.name.first;
        let last = user.name.last;
        return {
          id: uid,
          name: `${last.toUpperCase()}, ${first}`,
        };
      })
  );
export { getCurrentLetter, getLeftPosition, getNames };
