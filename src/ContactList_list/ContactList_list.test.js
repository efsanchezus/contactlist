import React from 'react';
import List from './ContactList_list';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_list', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders last names starting by the letter passed via props', () => {
    const store = mockStore(initialState);
    const { getByText } = render(
      <Provider store={store}>
        <List letter="a" />
      </Provider>
    );
    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();
  });
});
