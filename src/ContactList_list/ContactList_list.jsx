import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Item from '../ContactList_item/ContactList_item';
import { getCurrentLetter, getLeftPosition, getNames } from './selectors';
import './ContactList_list.css';

function List({ letter }) {
  const getNamesMemo = useMemo(getNames, []),
    users = useSelector(state => getNamesMemo(state, letter)),
    currentLetter = useSelector(getCurrentLetter),
    leftPosition = useSelector(getLeftPosition(letter)),
    createItems = () =>
      users.map(user => <Item id={user.id} name={user.name} key={user.id} />);

  return (
    <div className="ContactList_list" style={{ left: `${leftPosition}px` }}>
      {currentLetter === letter && createItems()}
    </div>
  );
}

List.propTypes = {
  letter: PropTypes.string.isRequired,
};
export default List;
