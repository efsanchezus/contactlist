import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import reduxFetch from '../middleware/redux-fetch';
import timekeeper from 'timekeeper';
import * as types from '../actions/const';
import * as actions from '../actions/index';
const middlewares = [reduxFetch, thunk],
  mockStore = configureStore(middlewares);

describe('actions', () => {
  let time;
  beforeEach(() => {
    time = new Date(1451935054510);
    timekeeper.freeze(time);
  });
  afterEach(() => {
    timekeeper.reset();
    fetchMock.restore();
    localStorage.removeItem('didFetch');
  });

  it('should not create CONTACT_LIST_FETCH_USERS_REQUEST when \
  	  contact_list.isLoading and contact_list.didFetch are true', () => {
    fetchMock.mock('/users', 200);

    const store = mockStore({
      contact_list: { didFetch: true, isLoading: true },
    });
    expect(
      store.dispatch(actions.contactList_fetchUsers('users', '', 1))
    ).toEqual(undefined);
  });

  it('should not create CONTACT_LIST_FETCH_USERS_REQUEST when \
  	  contact_list.isLoading is true', () => {
    fetchMock.mock('/users', 200);

    const store = mockStore({
      contact_list: { didFetch: false, isLoading: true },
    });
    expect(
      store.dispatch(actions.contactList_fetchUsers('users', '', 1))
    ).toEqual(undefined);
  });
  it('should not create CONTACT_LIST_FETCH_USERS_REQUEST when \
  	  contact_list.didFetch is true', () => {
    fetchMock.mock('/users', 200);

    const store = mockStore({
      contact_list: { didFetch: true, isLoading: false },
    });
    expect(
      store.dispatch(actions.contactList_fetchUsers('users', '', 1))
    ).toEqual(undefined);
  });

  it('should create CONTACT_LIST_FETCH_USERS_FAILURE when \
  	  fetching has failed and remove from localStorage didFetch', () => {
    fetchMock.mock('/users', 404);

    const expectedActions = [
      { type: types.CONTACT_LIST_FETCH_USERS_REQUEST, payload: {} },
      {
        type: types.CONTACT_LIST_FETCH_USERS_FAILURE,
        error: true,
        payload: { error: new Error('Network connection error') },
      },
    ];

    const store = mockStore({
      contact_list: { didFetch: false, isLoading: false },
    });
    expect(localStorage.getItem('didFetch')).toEqual(null);
    return store
      .dispatch(actions.contactList_fetchUsers('users', '', 1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
      .then(() => expect(localStorage.getItem('didFetch')).toEqual(null));
  });

  it('should create CONTACT_LIST_FETCH_USERS_RECEIVE when \
  	  fetching has been done and save in localStorage \
  	  didFetch is true', () => {
    fetchMock.getOnce('/users', { body: { results: [] } });

    const expectedActions = [
      { type: types.CONTACT_LIST_FETCH_USERS_REQUEST, payload: {} },
      {
        type: types.CONTACT_LIST_FETCH_USERS_RECEIVE,
        payload: {
          data: {
            results: [],
          },
          receivedAt: time.getTime(),
        },
      },
    ];

    const store = mockStore({
      contact_list: { didFetch: false, isLoading: false },
    });

    expect(localStorage.getItem('didFetch')).toEqual(null);
    return store
      .dispatch(actions.contactList_fetchUsers('users', '', 1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
      .then(() => expect(localStorage.getItem('didFetch')).toEqual('true'));
  });

  it('should create an action to store the viewport width', () => {
    const expectedAction = {
      type: types.GENERAL_CHANGE_WIDTH,
      payload: { width: 1 },
    };
    expect(actions.general_changeWidth({ width: 1 })).toEqual(expectedAction);
  });

  it('should create an action to change the current letter', () => {
    const expectedAction = {
      type: types.CONTACT_LIST_CHANGE_LETTER,
      payload: { letter: 'a' },
    };
    expect(actions.contactList_changeLetter('a')).toEqual(expectedAction);
  });
  it('should create an action to change the current letter', () => {
    const expectedAction = {
      type: types.CONTACT_LIST_CHANGE_LETTER,
      payload: { letter: 'a' },
    };
    expect(actions.contactList_changeLetter('a')).toEqual(expectedAction);
  });
  it('should create an action to calculate the width of each list', () => {
    const expectedAction = {
      type: types.CONTACT_LIST_CALCULATE_LISTS_WIDTH,
      payload: { width: 1 },
    };
    expect(actions.contactList_calculateListsWidth(1)).toEqual(expectedAction);
  });
  it('should create an action to change the current user', () => {
    const expectedAction = {
      type: types.CONTACT_LIST_CHANGE_CURRENT_USER,
      payload: { id: 1 },
    };
    expect(actions.contactList_changeCurrentUser(1)).toEqual(expectedAction);
  });
  it('should create an action to clear the current user', () => {
    const expectedAction = {
      type: types.CONTACT_LIST_CLEAR_CURRENT_USER,
    };
    expect(actions.contactList_clearCurrentUser()).toEqual(expectedAction);
  });
});
