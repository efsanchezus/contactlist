export const CONTACT_LIST_CHANGE_LETTER = 'contactList/changeLetter';
export const CONTACT_LIST_FETCH_USERS_REQUEST = 'contactList/fetchUsersRequest';
export const CONTACT_LIST_FETCH_USERS_RECEIVE = 'contactList/fetchUsersReceive';
export const CONTACT_LIST_FETCH_USERS_FAILURE = 'contactList/fetchUsersFailure';
export const CONTACT_LIST_CALCULATE_LISTS_WIDTH =
  'contactList/calculateListsWidth';
export const CONTACT_LIST_CHANGE_CURRENT_USER = 'contactList/changeCurrentUser';
export const CONTACT_LIST_CLEAR_CURRENT_USER = 'contactList/clearCurrentUser';
export const TEST_STATUS = 'contactList/testStatus';
export const GENERAL_CHANGE_WIDTH = 'general/changeWidth';
