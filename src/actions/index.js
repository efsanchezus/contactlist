import {
  CONTACT_LIST_CHANGE_LETTER,
  CONTACT_LIST_FETCH_USERS_REQUEST,
  CONTACT_LIST_FETCH_USERS_RECEIVE,
  CONTACT_LIST_FETCH_USERS_FAILURE,
  CONTACT_LIST_CALCULATE_LISTS_WIDTH,
  CONTACT_LIST_CHANGE_CURRENT_USER,
  CONTACT_LIST_CLEAR_CURRENT_USER,
  TEST_STATUS,
  GENERAL_CHANGE_WIDTH,
} from './const';

export const fetchThenCreateLists_thunk = (host, params, width) => dispatch => {
  let dispatchFetchUsers = dispatch(contactList_fetchUsers(host, params));
  if (dispatchFetchUsers === undefined) {
    // Users in localStorage, calculate lists width
    dispatch(contactList_calculateListsWidth(width));
    return;
  }

  dispatchFetchUsers
    .then(() => dispatch(contactList_calculateListsWidth(width)))
    .catch(error => console.error('My error', error));
};

const shouldFetchUsers = state => {
  const { didFetch, isLoading } = state.contact_list;
  if (isLoading) {
    return false;
  } else if (didFetch) {
    return false;
  } else {
    return true;
  }
};
export const contactList_fetchUsers = (host, params) => {
  window.localStorage.setItem('didFetch', true);
  return {
    types: [
      CONTACT_LIST_FETCH_USERS_REQUEST,
      CONTACT_LIST_FETCH_USERS_RECEIVE,
      CONTACT_LIST_FETCH_USERS_FAILURE,
    ],
    callAPI: () => fetch(`${host}${params}`),
    shouldCallAPI: state => shouldFetchUsers(state),
    persistAs: 'users',
  };
};

export const contactList_changeLetter = letter => ({
  type: CONTACT_LIST_CHANGE_LETTER,
  payload: { letter },
});

export const contactList_calculateListsWidth = width => ({
  type: CONTACT_LIST_CALCULATE_LISTS_WIDTH,
  payload: { width },
});

export const contactList_changeCurrentUser = id => ({
  type: CONTACT_LIST_CHANGE_CURRENT_USER,
  payload: { id },
});

export const contactList_clearCurrentUser = () => ({
  type: CONTACT_LIST_CLEAR_CURRENT_USER,
});

export const contactList_testStatus = payload => ({
  type: TEST_STATUS,
  payload,
});

export const general_changeWidth = payload => ({
  type: GENERAL_CHANGE_WIDTH,
  payload,
});
