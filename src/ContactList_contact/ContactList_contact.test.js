import React from 'react';
import Contact from './ContactList_contact';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_contact', () => {
  afterEach(() => {
    cleanup();
  });

  it('shows data from users', () => {
    const store = mockStore({
      contact_list: {
        ...initialState.contact_list,
        currentUser: '9832ff03-ef05-4614-877f-ce6787bd9ccf',
      },
    });
    const { getByText, queryByText } = render(
      <Provider store={store}>
        <Contact />
      </Provider>
    );

    expect(getByText(/brewer felecia/i)).toBeInTheDocument();
    expect(getByText(/felecia.brewer@example.com/i)).toBeInTheDocument();
    expect(queryByText('(032)-501-4457')).toBeInTheDocument();
  });
});
