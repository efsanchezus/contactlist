import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import ButtonClose from '../ContactList_btnClose/ContactList_btnClose';
import Username from '../ContactList_username/ContactList_username';
import Avatar from '../ContactList_avatar/ContactList_avatar';
import Item from '../ContactList_contact_item/ContactList_contact_item';
import PropTypes from 'prop-types';
import './ContactList_contact.css';

const defaultUser = {
  name: {},
  login: {},
  picture: {},
  location: {
    street: {},
  },
};

function Contact({ isMobile = false }) {
  const [user, setUser] = useState(defaultUser),
    [expandedClass, setExpandedClass] = useState(''),
    [typeClass, setTypeClass] = useState(''),
    usersById = useSelector(state => state.contact_list.usersById),
    currentUser = useSelector(state => state.contact_list.currentUser),
    baseClass = 'ContactList_contact absolute-element',
    [name, setName] = useState(''),
    [email, setEmail] = useState(''),
    [phone, setPhone] = useState(''),
    [address, setAddress] = useState(''),
    [city, setCity] = useState(''),
    [state, setState] = useState(''),
    [country, setCountry] = useState(''),
    [postcode, setPostcode] = useState('');

  useEffect(() => {
    if (currentUser) {
      setExpandedClass(`ContactList_contact--expand`);
      setUser(usersById[currentUser]);
      return;
    }
    setUser(defaultUser);
    setExpandedClass(`ContactList_contact--collapse`);
    document.title = 'Contact list';
  }, [currentUser]);

  useEffect(() => {
    if (isMobile) {
      setTypeClass(`ContactList_contact-mobile`);
      return;
    }
    setTypeClass(`ContactList_contact-desktop`);
  }, [isMobile]);

  useEffect(() => {
    if (user.name.last) {
      document.title = `${user.name.last} ${user.name.first}`;

      setName(`${user.name.last} ${user.name.first}`);
      setEmail(user.email);
      setPhone(user.phone);
      setAddress(`${user.location.street.number} 
                  ${user.location.street.name}`);
      setCity(user.location.city);
      setState(user.location.state);
      setCountry(user.location.country);
      setPostcode(user.location.postcode);
    }
  }, [user]);
  return (
    <div className={`${baseClass} ${typeClass} ${expandedClass}`}>
      <ButtonClose />
      <div className="ContactList_contact-left">
        <Avatar picture={user.picture} />
      </div>
      <div className="ContactList_contact-right">
        <h3 className="ContactList_contact-name">{name}</h3>
        <ul className="ContactList_contact-list">
          <Item topic="Email" value={email} />
          <Item topic="Phone" value={phone} />
          <Item topic="Street" value={address} />
          <Item topic="City" value={city} />
          <Item topic="State" value={state} />
          <Item topic="Country" value={country} />
          <Item topic="Postcode" value={postcode} />
        </ul>
      </div>
      <Username text={user.login.username} />
    </div>
  );
}

Contact.propTypes = {
  isMobile: PropTypes.bool,
};

export default Contact;
