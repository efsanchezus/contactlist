import React from 'react';
import { useSelector } from 'react-redux';
import Letter from '../ContactList_letter_mobile/ContactList_letter_mobile';
import './ContactList_content_mobile.css';

function Content() {
  const letters = useSelector(state => state.contact_list.lettersWithData),
    createLetters = () =>
      letters.map(letter => <Letter letter={letter} key={letter} />);

  return <div className="ContactList_content_mobile">{createLetters()}</div>;
}

export default Content;
