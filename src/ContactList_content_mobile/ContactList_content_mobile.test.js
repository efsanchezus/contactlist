import React from 'react';
import Content from './ContactList_content_mobile';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_content_mobile', () => {
  afterEach(() => {
    cleanup();
  });

  it('shows letters and contact names', () => {
    const store = mockStore({
      contact_list: { ...initialState.contact_list, currentLetter: 'a' },
      general: { width: 600 },
    });
    const { getByText, queryByText } = render(
      <Provider store={store}>
        <Content />
      </Provider>
    );

    expect(getByText('a')).toBeInTheDocument();
    expect(getByText('b')).toBeInTheDocument();

    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();
    expect(getByText(/brewer/i)).toBeInTheDocument();
  });
});
