import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers/root';
import thunk from 'redux-thunk';
import reduxFetch from './middleware/redux-fetch';
import { createLogger } from 'redux-logger';
import monitorReducersEnhancer from './enhancers/monitorReducer';

const logger = createLogger();
const composeDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const configureStore = preloadedState => {
  const middlewares = [thunk, reduxFetch],
    middlewareEnhancer = applyMiddleware(...middlewares),
    enhancers = [monitorReducersEnhancer, middlewareEnhancer],
    composedEnhancers = composeDevTools(...enhancers),
    store = createStore(rootReducer, preloadedState, composedEnhancers);

  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./reducers/root', () =>
      store.replaceReducer(rootReducer)
    );
  }

  return store;
};
const store = configureStore();

ReactDOM.render(<App store={store} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
