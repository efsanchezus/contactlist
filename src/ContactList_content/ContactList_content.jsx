import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { translateToSelector, lettersWithDataSelector } from './selectors';
import List from '../ContactList_list/ContactList_list';
import './ContactList_content.css';

function Content() {
  const translateTo = useSelector(translateToSelector),
    letters = useSelector(lettersWithDataSelector),
    [translate, setTranslate] = useState(`translateX(-${translateTo}px)`),
    createLetters = () =>
      letters.map(letter => <List key={letter} letter={letter} />);

  useEffect(() => setTranslate(`translateX(-${translateTo}px)`), [translateTo]);
  return (
    <div className="ContactList_content" style={{ transform: translate }}>
      {createLetters()}
    </div>
  );
}

export default Content;
