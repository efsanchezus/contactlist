import React from 'react';
import Content from './ContactList_content';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_content', () => {
  afterEach(() => {
    cleanup();
  });

  it('shows contact names', () => {
    const store = mockStore({
      contact_list: { ...initialState.contact_list, currentLetter: 'a' },
      general: { width: 900 },
    });
    const { getByText, queryByText } = render(
      <Provider store={store}>
        <Content />
      </Provider>
    );

    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();
    expect(queryByText(/brewer/i)).toBeNull();
  });
});
