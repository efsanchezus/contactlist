const translateToSelector = state => state.contact_list.translateTo;
const lettersWithDataSelector = state =>
  state.contact_list.lettersWithData || [];

export { translateToSelector, lettersWithDataSelector };
