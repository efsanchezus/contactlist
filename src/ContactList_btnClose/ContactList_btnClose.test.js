import React from 'react';
import ButtonClose from './ContactList_btnClose';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
const mockStore = configureStore();
const store = mockStore();
store.dispatch = jest.fn();

describe('ContactList_btnClose', () => {
  let getByText;
  beforeEach(() => {
    ({ getByText } = render(
      <Provider store={store}>
        <ButtonClose />
      </Provider>
    ));
  });

  afterEach(() => {
    cleanup();
  });

  it('renders a cross mark and  calls dispatch when tab is clicked', () => {
    expect(getByText('✕')).toBeInTheDocument();

    fireEvent.click(getByText('✕'));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
