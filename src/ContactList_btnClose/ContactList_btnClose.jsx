import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { contactList_clearCurrentUser } from '../actions';
import './ContactList_btnClose.css';

function ButtonClose() {
  const dispatch = useDispatch(),
    clearCurrentUser = () => dispatch(contactList_clearCurrentUser()),
    callback = useCallback(clearCurrentUser, []);

  return (
    <div className="ContactList_btnClose" onClick={callback}>
      <span>&#10005;</span>
      <div className="ContactList_btnClose-background absolute-element"></div>
    </div>
  );
}

export default ButtonClose;
