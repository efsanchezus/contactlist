import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './ContactList_avatar.css';

function Avatar({ picture }) {
  const [showLoader, setShowLoader] = useState(''),
    [backgroundImage, setBackgroundImage] = useState('none'),
    image = new Image(),
    loaderCSS = 'ContactList_avatar-loader absolute-element';
  let url;

  useEffect(() => {
    switch (navigator.connection.effectiveType) {
      case 'slow-2g':
      case '2g': {
        url = picture.thumbnail;
        break;
      }
      case '3g': {
        url = picture.medium;
        break;
      }
      default: {
        url = picture.large;
      }
    }

    if (Object.keys(picture).length > 0) {
      setShowLoader('ContactList_avatar-loader--show');
      image.src = url;
      image.onload = () => {
        setBackgroundImage(`url(${url})`);
        setShowLoader('ContactList_avatar-loader--hide');
      };
    } else {
      setBackgroundImage('none');
      setShowLoader('ContactList_avatar-loader--hide');
    }
  }, [picture]);

  return (
    <div className="ContactList_avatar">
      <div
        className="ContactList_avatar-img absolute-element"
        style={{ backgroundImage }}
      />
      <div className={`${loaderCSS} ${showLoader}`} />}
    </div>
  );
}

Avatar.propTypes = {
  picture: PropTypes.object.isRequired,
};
export default Avatar;
