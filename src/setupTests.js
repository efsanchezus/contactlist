import '@testing-library/jest-dom/extend-expect';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
global.navigator.connection = {
  effectiveType: '',
};

export const initialState = {
  contact_list: {
    didFetch: true,
    usersById: {
      '9832ff03-ef05-4614-877f-ce6787bd9ccf': {
        gender: 'female',
        name: {
          title: 'Miss',
          first: 'Felecia',
          last: 'Brewer',
        },
        location: {
          street: {
            number: 2315,
            name: 'Rolling Green Rd',
          },
          city: 'Omaha',
          state: 'Minnesota',
          country: 'United States',
          postcode: 61064,
          coordinates: {
            latitude: '-66.2680',
            longitude: '129.6636',
          },
          timezone: {
            offset: '-3:00',
            description: 'Brazil, Buenos Aires, Georgetown',
          },
        },
        email: 'felecia.brewer@example.com',
        login: {
          uuid: '9832ff03-ef05-4614-877f-ce6787bd9ccf',
          username: 'smallpanda109',
          password: 'secret',
          salt: '79yr6099',
          md5: '8dd0b898865ba8fa1dc795b81b9ac3aa',
          sha1: 'a520c71a88193c811e7add5f9b969e8193b9840e',
          sha256:
            '40564f9d6e42488979d6d177efcabd45eb0bb5fbdbec4f7a0ae0524a73ebbcca',
        },
        dob: {
          date: '1956-07-11T05:17:27.733Z',
          age: 63,
        },
        registered: {
          date: '2008-07-30T03:25:36.726Z',
          age: 11,
        },
        phone: '(032)-501-4457',
        cell: '(947)-275-8160',
        id: {
          name: 'SSN',
          value: '653-61-2192',
        },
        picture: {
          large: 'https://randomuser.me/api/portraits/women/71.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/71.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/71.jpg',
        },
        nat: 'US',
      },
      '982a5a8c-3f19-4ef2-bef8-9e79be44c7d8': {
        gender: 'female',
        name: {
          title: 'Mrs',
          first: 'Miriam',
          last: 'Brooks',
        },
        location: {
          street: {
            number: 580,
            name: 'Groveland Terrace',
          },
          city: 'Richardson',
          state: 'Pennsylvania',
          country: 'United States',
          postcode: 39013,
          coordinates: {
            latitude: '-60.3460',
            longitude: '1.9981',
          },
          timezone: {
            offset: '+5:30',
            description: 'Bombay, Calcutta, Madras, New Delhi',
          },
        },
        email: 'miriam.brooks@example.com',
        login: {
          uuid: '982a5a8c-3f19-4ef2-bef8-9e79be44c7d8',
          username: 'silverpeacock594',
          password: 'baberuth',
          salt: 'hw6G0aB0',
          md5: 'e292687137a603a64dfb742270995f95',
          sha1: '55d95885d23bd0534e8ad8e6def693cb10247aeb',
          sha256:
            '9f93ceb75a2cc594befa3e17229d394f9fa1c8d1cc2d51c59d73e57a651f4772',
        },
        dob: {
          date: '1975-04-20T12:25:25.329Z',
          age: 44,
        },
        registered: {
          date: '2003-11-25T04:01:23.913Z',
          age: 16,
        },
        phone: '(249)-072-0834',
        cell: '(073)-016-6043',
        id: {
          name: 'SSN',
          value: '191-90-6463',
        },
        picture: {
          large: 'https://randomuser.me/api/portraits/women/91.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/91.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/91.jpg',
        },
        nat: 'US',
      },
      'b8763ec8-22b6-48e6-9732-7a2e14249bee': {
        gender: 'female',
        name: {
          title: 'Ms',
          first: 'Sally',
          last: 'Alvarez',
        },
        location: {
          street: {
            number: 1481,
            name: 'Country Club Rd',
          },
          city: 'Caldwell',
          state: 'New Hampshire',
          country: 'United States',
          postcode: 43659,
          coordinates: {
            latitude: '-51.8165',
            longitude: '13.0673',
          },
          timezone: {
            offset: '+5:30',
            description: 'Bombay, Calcutta, Madras, New Delhi',
          },
        },
        email: 'sally.alvarez@example.com',
        login: {
          uuid: 'b8763ec8-22b6-48e6-9732-7a2e14249bee',
          username: 'purpleduck133',
          password: 'gocubs',
          salt: 'MrksAu0M',
          md5: '975ed66dc87990ea8f23f014c65934a3',
          sha1: '3ad189b35a7f07d14bfee214524497c277ca8902',
          sha256:
            '2dbf57787d1a529fba20a33aa55ad417fd9f77a9e67c91fc2bcb4d47b8a15fcc',
        },
        dob: {
          date: '1952-11-19T00:56:58.419Z',
          age: 67,
        },
        registered: {
          date: '2009-03-11T14:34:08.357Z',
          age: 10,
        },
        phone: '(937)-208-4782',
        cell: '(085)-089-3736',
        id: {
          name: 'SSN',
          value: '189-32-0510',
        },
        picture: {
          large: 'https://randomuser.me/api/portraits/women/33.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/33.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/33.jpg',
        },
        nat: 'US',
      },
      '9dfe0ee6-f2c1-48ce-aae1-f22be5a8a65d': {
        gender: 'male',
        name: {
          title: 'Mr',
          first: 'Bryan',
          last: 'Anderson',
        },
        location: {
          street: {
            number: 6149,
            name: 'Taylor St',
          },
          city: 'Clarksville',
          state: 'Wyoming',
          country: 'United States',
          postcode: 23274,
          coordinates: {
            latitude: '64.3892',
            longitude: '104.1935',
          },
          timezone: {
            offset: '-4:00',
            description: 'Atlantic Time (Canada), Caracas, La Paz',
          },
        },
        email: 'bryan.anderson@example.com',
        login: {
          uuid: '9dfe0ee6-f2c1-48ce-aae1-f22be5a8a65d',
          username: 'crazypanda797',
          password: 'oceans',
          salt: 'c6EEDUHY',
          md5: 'ee2b91ca3bcbe51d17aa63f8089b8106',
          sha1: '0addcd3809c3e0296e0c8f521c4d1ff5f70a4d87',
          sha256:
            '264dc3d2530023e1d00a39249e55baa8787b5773b49794d9db8c46a28444a7cf',
        },
        dob: {
          date: '1980-08-08T23:02:07.735Z',
          age: 39,
        },
        registered: {
          date: '2018-09-17T05:17:06.166Z',
          age: 1,
        },
        phone: '(822)-838-8950',
        cell: '(719)-919-3971',
        id: {
          name: 'SSN',
          value: '830-48-3845',
        },
        picture: {
          large: 'https://randomuser.me/api/portraits/men/30.jpg',
          medium: 'https://randomuser.me/api/portraits/med/men/30.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/men/30.jpg',
        },
        nat: 'US',
      },
    },
    usersByLetter: {
      b: [
        '982a5a8c-3f19-4ef2-bef8-9e79be44c7d8',
        '9832ff03-ef05-4614-877f-ce6787bd9ccf',
      ],
      a: [
        'b8763ec8-22b6-48e6-9732-7a2e14249bee',
        '9dfe0ee6-f2c1-48ce-aae1-f22be5a8a65d',
      ],
    },
    isLoading: false,
    error: null,
    currentLetter: 'a',
    lettersWithData: ['a', 'b'],
    listsWidth: {
      a: 0,
      b: 800,
    },
    translateTo: 8000,
    currentUser: null,
  },
  general: {
    width: 819,
  },
};
