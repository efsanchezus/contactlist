import React from 'react';
import Letter from './ContactList_letter_mobile';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render, fireEvent } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_letter_mobile', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders the letter and last names', () => {
    const store = mockStore(initialState);
    store.dispatch = jest.fn();
    const { getByText } = render(
      <Provider store={store}>
        <Letter letter="a" />
      </Provider>
    );
    expect(getByText('a')).toBeInTheDocument();
    expect(getByText(/alvarez/i)).toBeInTheDocument();
    expect(getByText(/anderson/i)).toBeInTheDocument();
  });
});
