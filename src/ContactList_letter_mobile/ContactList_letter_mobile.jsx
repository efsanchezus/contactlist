import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Item from '../ContactList_item_mobile/ContactList_item_mobile';
import './ContactList_letter_mobile.css';

function Letter({ letter }) {
  const users = useSelector(state => state.contact_list.usersByLetter[letter]),
    createItems = () => users.map(user => <Item key={user} id={user} />);
  return (
    <div className="ContactList_letter_mobile">
      <div className="ContactList_letter_mobile-header">{letter}</div>
      <div className="ContactList_letter_mobile-contacts">{createItems()}</div>
    </div>
  );
}

Letter.propTypes = {
  letter: PropTypes.string.isRequired,
};
export default Letter;
