import React from 'react';
import Content from '../ContactList_content_mobile/ContactList_content_mobile';
import Contact from '../ContactList_contact/ContactList_contact';
import Status from '../ContactList_status/ContactList_status';
import './ContactList_main_mobile.css';

function Main() {
  return (
    <main className="ContactList_main_mobile">
      <Content />
      <Contact isMobile={true} />
      <Status />
    </main>
  );
}

export default Main;
