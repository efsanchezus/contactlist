import React from 'react';
import * as actions from '../actions/const';
import reducer from './contact_list';

describe('contact_list reducer', () => {
  it('should handle CONTACT_LIST_CHANGE_LETTER', () => {
    expect(
      reducer(
        {
          listsWidth: {
            a: 10,
          },
        },
        {
          type: actions.CONTACT_LIST_CHANGE_LETTER,
          payload: {
            letter: 'a',
          },
        }
      )
    ).toEqual({
      listsWidth: {
        a: 10,
      },
      translateTo: 10,
      currentLetter: 'a',
    });
  });
  it('should handle CONTACT_LIST_FETCH_USERS_RECEIVE', () => {
    const defaultUser = {
      name: { last: 'Sánchez', first: 'Emiliano' },
      login: { uuid: 'asd123' },
      picture: {},
      location: {
        street: {},
      },
    };
    expect(
      reducer(
        {},
        {
          type: actions.CONTACT_LIST_FETCH_USERS_RECEIVE,
          payload: {
            data: {
              results: [defaultUser],
            },
          },
        }
      )
    ).toEqual({
      isLoading: false,
      error: undefined,
      lettersWithData: ['s'],
      usersById: {
        asd123: {
          location: {
            street: {},
          },
          login: {
            uuid: 'asd123',
          },
          name: {
            first: 'Emiliano',
            last: 'Sánchez',
          },
          picture: {},
        },
      },
      usersByLetter: {
        s: ['asd123'],
      },
    });
  });
  it('should handle CONTACT_LIST_FETCH_USERS_REQUEST', () => {
    expect(
      reducer(
        {},
        {
          type: actions.CONTACT_LIST_FETCH_USERS_REQUEST,
        }
      )
    ).toEqual({
      error: undefined,
      isLoading: true,
    });
  });
  it('should handle CONTACT_LIST_FETCH_USERS_FAILURE', () => {
    expect(
      reducer(
        {},
        {
          type: actions.CONTACT_LIST_FETCH_USERS_FAILURE,
          error: true,
          payload: { error: 'Network Connection Error' },
        }
      )
    ).toEqual({
      error: true,
      isLoading: false,
    });
  });
  it('should handle CONTACT_LIST_CALCULATE_LISTS_WIDTH', () => {
    expect(
      reducer(
        { lettersWithData: ['a', 'b'] },
        {
          type: actions.CONTACT_LIST_CALCULATE_LISTS_WIDTH,
          payload: { width: 10 },
        }
      )
    ).toEqual({
      lettersWithData: ['a', 'b'],
      listsWidth: {
        a: 0,
        b: 10,
      },
    });
  });
  it('should handle CONTACT_LIST_CHANGE_CURRENT_USER', () => {
    expect(
      reducer(
        {},
        {
          type: actions.CONTACT_LIST_CHANGE_CURRENT_USER,
          payload: { id: '1' },
        }
      )
    ).toEqual({ currentUser: '1' });
  });
  it('should handle CONTACT_LIST_CLEAR_CURRENT_USER', () => {
    expect(
      reducer({}, { type: actions.CONTACT_LIST_CLEAR_CURRENT_USER })
    ).toEqual({ currentUser: null });
  });
});
