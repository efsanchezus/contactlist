import { GENERAL_CHANGE_WIDTH } from '../actions/const';

const initialState = {
  width: null,
};
const general = (state = initialState, action) => {
  switch (action.type) {
    case GENERAL_CHANGE_WIDTH:
      return {
        ...state,
        width: action.payload.width,
      };
    default:
      return state;
  }
};

export default general;
