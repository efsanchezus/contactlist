import React from 'react';
import * as actions from '../actions/const';
import reducer from './general';

describe('general reducer', () => {
  it('should handle GENERAL_CHANGE_WIDTH', () => {
    expect(
      reducer(
        {},
        {
          type: actions.GENERAL_CHANGE_WIDTH,
          payload: { width: 100 },
        }
      )
    ).toEqual({ width: 100 });
  });
});
