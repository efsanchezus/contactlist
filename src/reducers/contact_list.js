import {
  CONTACT_LIST_CHANGE_LETTER,
  CONTACT_LIST_FETCH_USERS_REQUEST,
  CONTACT_LIST_FETCH_USERS_RECEIVE,
  CONTACT_LIST_FETCH_USERS_FAILURE,
  CONTACT_LIST_CALCULATE_LISTS_WIDTH,
  CONTACT_LIST_CHANGE_CURRENT_USER,
  CONTACT_LIST_CLEAR_CURRENT_USER,
  TEST_STATUS,
} from '../actions/const';

const calculateWidth = (letters, adder) => {
  let obj = {},
    currentWidth = 0;

  letters.forEach(letter => {
    obj[letter] = currentWidth;
    currentWidth += adder;
  });
  return obj;
};
const normalizeUsers = data => {
  let obj = {};

  if (typeof data === 'string') {
    data = JSON.parse(data);
  }
  data.results.forEach(user => (obj[user.login.uuid] = user));
  return obj;
};
const getUsersByLetter = data => {
  if (typeof data === 'string') {
    data = JSON.parse(data);
  }

  let obj = {};
  let letter;
  Object.values(data.results).forEach(user => {
    letter = user.name.last.slice(0, 1).toLowerCase();
    if (obj[letter]) {
      obj[letter] = [...obj[letter], user.login.uuid];
    } else {
      obj[letter] = [user.login.uuid];
    }
  });

  return obj;
};
const getLettersWithData = data => {
  if (typeof data === 'string') {
    data = JSON.parse(data);
  }

  let obj = {},
    letters = [],
    letter = null;

  Object.values(data.results).forEach(user => {
    letter = user.name.last.slice(0, 1).toLowerCase();
    obj[letter] = true;
  });

  letters = Object.keys(obj).sort();
  return letters;
};

const didFetch =
  window.localStorage.getItem('didFetch') === 'true' ? true : false;
const users = window.localStorage.getItem('users') || undefined;
const usersById = users !== undefined ? normalizeUsers(users) : {};
const usersByLetter = users !== undefined ? getUsersByLetter(users) : {};
const lettersWithData = users !== undefined ? getLettersWithData(users) : [];

const initialState = {
  didFetch,
  usersById,
  usersByLetter,
  isLoading: false,
  error: null,
  currentLetter: null,
  lettersWithData,
  listsWidth: {},
  translateTo: 0,
  currentUser: null,
};
const contact_list = (state = initialState, action) => {
  switch (action.type) {
    case CONTACT_LIST_CHANGE_LETTER:
      return {
        ...state,
        currentLetter: action.payload.letter,
        translateTo: state.listsWidth[action.payload.letter] || 0,
      };
    case CONTACT_LIST_FETCH_USERS_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: action.error,
      };
    case CONTACT_LIST_FETCH_USERS_RECEIVE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
        usersById: normalizeUsers(action.payload.data),
        usersByLetter: getUsersByLetter(action.payload.data),
        lettersWithData: getLettersWithData(action.payload.data),
      };
    case CONTACT_LIST_FETCH_USERS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    case CONTACT_LIST_CALCULATE_LISTS_WIDTH:
      return {
        ...state,
        listsWidth: calculateWidth(state.lettersWithData, action.payload.width),
      };
    case CONTACT_LIST_CHANGE_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload.id,
      };
    case CONTACT_LIST_CLEAR_CURRENT_USER:
      return {
        ...state,
        currentUser: null,
      };
    case TEST_STATUS:
      return {
        ...state,
        error: action.payload.error || null,
        isLoading: action.payload.isLoading || false,
      };
    default:
      return state;
  }
};

export default contact_list;
