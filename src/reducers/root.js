import { combineReducers } from 'redux';
import general from './general';
import contact_list from './contact_list';

const rootReducer = combineReducers({
  contact_list,
  general,
});

export default rootReducer;
