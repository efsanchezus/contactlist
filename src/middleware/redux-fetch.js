export default function reduxFetch({ dispatch, getState }) {
  return next => action => {
    const {
      types,
      shouldCallAPI = () => true,
      callAPI,
      payload = {},
      persistAs,
    } = action;

    if (!types) {
      return next(action);
    }

    if (
      !Array.isArray(types) ||
      types.length !== 3 ||
      !types.every(type => typeof type === 'string')
    ) {
      throw new Error('Expected an array of three string types');
    }
    if (typeof callAPI !== 'function') {
      throw new Error('Expected callAPI to be a function');
    }
    if (!shouldCallAPI(getState())) {
      return;
    }

    const [requestType, successType, failureType] = types;
    dispatch({
      type: requestType,
      payload: { ...payload },
    });
    return callAPI()
      .then(
        response => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Network connection error');
          }
        },
        error => {
          throw new Error(error);
        }
      )
      .then(
        data => {
          if (persistAs !== undefined && typeof persistAs === 'string') {
            window.localStorage.setItem(persistAs, JSON.stringify(data));
          }

          dispatch({
            type: successType,
            payload: { ...payload, data, receivedAt: Date.now() },
          });
        },
        error => {
          window.localStorage.removeItem('didFetch');

          dispatch({
            type: failureType,
            payload: { ...payload, error },
            error: true,
          });
        }
      );
  };
}
