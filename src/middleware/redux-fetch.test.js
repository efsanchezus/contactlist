import reduxFetch from './redux-fetch';

const callAPI = function() {};
const shouldCallAPI = () => false;
const create = () => {
  const store = {
    getState: jest.fn(() => ({})),
    dispatch: jest.fn(),
  };
  const next = jest.fn();
  const invoke = action => reduxFetch(store)(next)(action);
  return { store, next, invoke };
};
const errors = {
  threeStrings: Error('Expected an array of three string types'),
  callAPI: Error('Expected callAPI to be a function'),
};

describe('redux-fetch', () => {
  it('passes to next if "types" is not in action', () => {
    const { next, invoke } = create();
    const action = { type: ['a', 'b', 'c'] };
    invoke(action);
    expect(next).toHaveBeenCalledWith(action);
  });
  it('throw an error if "types" is not an array', () => {
    const { invoke } = create();
    const action = { types: 'actions' };
    expect(() => invoke(action)).toThrow(errors.threeStrings);
  });
  it('throw an error if "types" length is different than 3', () => {
    const { invoke } = create();
    const action = { types: ['a', 'b'] };
    expect(() => invoke(action)).toThrow(errors.threeStrings);
  });
  it('throw an error if all elements in "types" are not strings', () => {
    const { invoke } = create();
    const action = { types: ['a', 'b', 1] };
    expect(() => invoke(action)).toThrow(errors.threeStrings);
  });
  it('throw an error if "callAPI" attribute is not a function', () => {
    const { invoke } = create();
    const action = { types: ['a', 'b', 'c'], callAPI: 'c' };
    expect(() => invoke(action)).toThrow(errors.callAPI);
  });
  it('return undefined if "shouldCallAPI" is set to false', () => {
    const { invoke } = create();
    const action = { types: ['a', 'b', 'c'], callAPI, shouldCallAPI };
    expect(invoke(action)).toEqual(undefined);
  });
});
