import React from 'react';
import Header from './ContactList_header';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { cleanup, render } from '@testing-library/react';
import { initialState } from '../setupTests';
const mockStore = configureStore();

describe('ContactList_content_mobile', () => {
  afterEach(() => {
    cleanup();
  });

  it('shows letters and contact names', () => {
    const store = mockStore(initialState);
    const { getByText, queryByText } = render(
      <Provider store={store}>
        <Header tabs={['a', 'b']} />
      </Provider>
    );

    expect(getByText('a')).toBeInTheDocument();
    expect(getByText('b')).toBeInTheDocument();
  });
});
