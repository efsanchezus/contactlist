import React from 'react';
import PropTypes from 'prop-types';
import Tab from '../ContactList_tab/ContactList_tab';
import './ContactList_header.css';

function Header({ tabs = [] }) {
  const createTabs = () => tabs.map(l => <Tab letter={l} key={l} />);
  return <div className="ContactList_header">{createTabs()}</div>;
}

Header.propTypes = {
  tabs: PropTypes.array.isRequired,
};
export default Header;
