import React from 'react';
import { Provider } from 'react-redux';
import ContactList from '../ContactList/ContactList';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
import ViewportSize from '../ViewportSize/ViewportSize';
import PropTypes from 'prop-types';
import './App.css';

const App = ({ store }) => {
  return (
    <Provider store={store}>
      <div className="App">
        <ErrorBoundary text="Something went wrong">
          <ContactList />
        </ErrorBoundary>
        <ViewportSize />
      </div>
    </Provider>
  );
};

App.propTypes = {
  store: PropTypes.object.isRequired,
};
export default App;
