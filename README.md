# Contact List App
## Brief
The app was developed with CRA and a fallback loader with plain HTML and CSS to speed up the first render since React is not light.

###Idea
The app wraps a `header` and `section`. 
In the `header` we can find the **Tabs**.
In the `section` we can find** Three Layers**.
  1. Contact. Shows the user's data
  2. Status. Shows if it's loading or there's an error
  3. Content. Contains the lists by letter

###Dependencies
`react-scripts` to make use of Create React App
`redux` to handle state of the application
`react-redux` to have access to redux hooks
`reselect` since useSelector hook performs strict comparison, it calls render if we return a new reference. (I don't use it, using .map or .filter will render every time)


###Testing
The testing approach that I have taken was avoiding* implementation details* to not get false negatives or false positives in future refactor.
I decided to use a `Testing Library React `which is a library that makes it difficult to implement.
Almost all the tests have mounted the components with render instead of the **shallow** since the latter one just tests the JSX object returned by the component instead of what the user actually sees.

####Unit Testing
As these components lack of business logic, they don't need to be unit tested.
App **(!)**
ContactList_username **(!)**
ContactList_avatar
ContactList_main 
ContactList_main_mobile

**(!)** Since final user is the developer, static testing with prop-types is enough

#####Components 
`ContactList_contact_item`
Since the final user is the developer I need to let them know that if they refactor the component, the outer marker must be an `li` for SEO Optimization

`ContactList_tab` 
I want to let developers know that it has to dispatch an action and users see the letter and a counter. The letter is static tested with prop-types

`ContactList_btnClose`
I want to let developers know that it has to dispatch an action and users see the cross mark.

`ContactList_status` 
Test that shows the proper message to the user

`ContactList_item` 
Shows the name of the user and dispatches an action

`ContactList_item_mobile` 
Shows the name of the user and dispatches dispatch

ContactList_list
ContactList_letter_mobile
ContactList_content_mobile
ContactList_content
ContactList_contact
ContactList_header

#####Non-Components
These files were tested following the Redux documentation

action
redux-fetch
general reducer
contact_list reducer

#####Pendings
ContactList
ViewportSize

###CSS Performance
All the style changes applied in the app were made using just **transform** and **opacity**.
Hover animations were made by creating a node with an initial style with a color and` opacity 0` so that when hovered it just changes to `opacity 1`

###Issues
Tab is not getting the proper styles in production, that's why I had to make a node with a class _trick_. When debugging in the browser I could see that the element had the class 'ContactList_tab' but it wasn't computing in the element styles.
